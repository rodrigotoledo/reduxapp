import { createStore, applyMiddleware } from 'redux';

import reducers from './reducers';

const middleware = [];

const createAppropriateStore = __DEV__ ? console.tron.createStore : createStore;

// desta maneira eu tenho da store com unico reducer que tem tudo
const store = createAppropriateStore(reducers, applyMiddleware(...middleware));

export default store;