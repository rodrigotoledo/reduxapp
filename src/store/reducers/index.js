import { combineReducers } from 'redux';

// sao reducers abaixo
import favorites from './favorites';

// aqui posso combinar favorites em um grande e unico
export default combineReducers({
  favorites,
});